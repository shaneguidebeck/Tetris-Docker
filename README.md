I want to deploy the tetris java app using docker & ECS to gain a better understanding of both docker and ECS.

Lessons Learned:
- Can't use `rules` keyword in gitlab-ci.yml on templates unless overriding jobs
- Better understanding of gitlab runners
- Brushed up a little on java
- Couldn't run the game in browser without additional code (Only runs locally)
- Used CI lint but couldn't get it to simulate a pipeline

## Details
A Java 17 Gradle Spring Boot App of Tetris

## Requirements
- [Java 17](https://adoptium.net/)

## Usage
- Build
  ```
  ./gradlew build
  ```
  - Output directory: `build/libs`
- Run
  ```
  ./gradlew bootRun
  ```
  or
  ```
  java -jar build/libs/tetris-0.0.1-SNAPSHOT.jar
  ```
- Test
  ```
  ./gradlew test

  ```
